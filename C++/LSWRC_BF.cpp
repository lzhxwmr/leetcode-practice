/*
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/

// This is the version implements by brute force using set
#include <iostream>
#include <string>
#include <unordered_set>
using namespace std;

class Solution {
public:
    bool allUnique(string s, int start, int end) {
        unordered_set<char> set;
        for (int i = start;i<end;++i) {
            char ch = s[i];
            if (set.find(ch) != set.end()) return false;
            set.insert(ch);
        }
        return true;
    } 

    int lengthOfLongestSubstring(string s) {
        const int n =s.length();
        int max = 0;
        for (int i = 0;i<n;++i) {
            for (int j=i+1;j<=n;++j) {
                if (allUnique(s,i,j)) max = (j-i)>max?(j-i):max;
            }
        }
        return max;
    }
};

string stringToString(string input) {
    assert(input.length() >= 2);
    string result;
    for (int i = 1; i < input.length() -1; i++) {
        char currentChar = input[i];
        if (input[i] == '\\') {
            char nextChar = input[i+1];
            switch (nextChar) {
                case '\"': result.push_back('\"'); break;
                case '/' : result.push_back('/'); break;
                case '\\': result.push_back('\\'); break;
                case 'b' : result.push_back('\b'); break;
                case 'f' : result.push_back('\f'); break;
                case 'r' : result.push_back('\r'); break;
                case 'n' : result.push_back('\n'); break;
                case 't' : result.push_back('\t'); break;
                default: break;
            }
            i++;
        } else {
        result.push_back(currentChar);
        }
    }
    return result;
}

int main() {
    string line;
    // while (getline(cin, line)) {
        // string s = stringToString(line);
        string s = "abcdefghijklmnopqrstuvwxyz";
        
        int ret = Solution().lengthOfLongestSubstring(s);

        string out = to_string(ret);
        cout << out << endl;
    // }
    return 0;
}