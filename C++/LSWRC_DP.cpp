// This is the version developed by myself and it adopts dynamic programming but run time exceeds
#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        const long int n = s.length()>0?s.length():1;
        int max=1;
        int OPT[n][n];
        for (long int len=0;len<n;++len) {
            for (long int j=n-1;j>=len;--j) {
                long int i = j-len;
                if (len==0) { OPT[i][j]=1; }
                else {
                    if ((j-1 >= 0 && OPT[i][j-1] == 0 )||(i+1<n && OPT[i+1][j] == 0)) OPT[i][j] = 0;
                    else {
                        int opt1,opt2;
                        string sub1 = s.substr(i,len);
                        string sub2 = s.substr(i+1,len);
                        char c1 = s[i];
                        char c2 = s[j];
                        if (sub1.find(c2)==string::npos) { opt1 = OPT[i+1][j]+1; }
                        else opt1 = 0;
                        if (sub2.find(c1)==string::npos) { opt2 = OPT[i][j-1]+1; }
                        else opt2 = 0;
                        OPT[i][j] = opt1>opt2?opt1:opt2;
                        max = OPT[i][j]>max?OPT[i][j]:max;
                    }
                }
            }
        }
        return max;
    }
};

string stringToString(string input) {
    assert(input.length() >= 2);
    string result;
    for (int i = 1; i < input.length() -1; i++) {
        char currentChar = input[i];
        if (input[i] == '\\') {
            char nextChar = input[i+1];
            switch (nextChar) {
                case '\"': result.push_back('\"'); break;
                case '/' : result.push_back('/'); break;
                case '\\': result.push_back('\\'); break;
                case 'b' : result.push_back('\b'); break;
                case 'f' : result.push_back('\f'); break;
                case 'r' : result.push_back('\r'); break;
                case 'n' : result.push_back('\n'); break;
                case 't' : result.push_back('\t'); break;
                default: break;
            }
            i++;
        } else {
        result.push_back(currentChar);
        }
    }
    return result;
}

int main() {
    string line;
    // while (getline(cin, line)) {
        // string s = stringToString(line);
        string s = "abcdefghijklmnopqrstuvwxyz";
        
        int ret = Solution().lengthOfLongestSubstring(s);

        string out = to_string(ret);
        cout << out << endl;
    // }
    return 0;
}