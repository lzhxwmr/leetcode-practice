class Solution:
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        set = set()
        ans=0
        i=0
        j=0
        while (i<n and j<n):
            if s[j] in set:
                set.add(s[j])
                ans = max(ans,j-1)
            else:
                set.remove(i)
                i=i+1
        return ans

    def stringToString(input):
        import json

        return json.loads(input)

    def main():
        import sys
        import io
        def readlines():
            for line in io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8'):
                yield line.strip('\n')

        lines = readlines()
        while True:
            try:
                line = next(lines)
                s = stringToString(line);
                
                ret = Solution().lengthOfLongestSubstring(s)

                out = str(ret);
                print(out)
            except StopIteration:
                break

    if __name__ == '__main__':
        main()