import java.util.LinkedList;
import java.util.Queue;

/*
 * 
 * Given a 32-bit signed integer, reverse digits of an integer.
 * 
 * Example 1:
 * 
 * Input: 123 Output: 321 
 * 
 * Example 2:
 * 
 * Input: -123 Output: -321 
 * 
 * Example 3:
 * 
 * Input: 120 Output: 21
 * 
 * Note: Assume we are dealing with an environment which could only store
 * integers within the 32-bit signed integer range: [−2^31, 2^31 − 1]. For the
 * purpose of this problem, assume that your function returns 0 when the
 * reversed integer overflows.
 */
class Solution {
    public int reverse(int x) {
        Queue<Integer> digits = new LinkedList<Integer>();
        int isPositive = 1;
        int left;
        int reversed = 0;
        if (x < 0) {
            left = -x;
            isPositive = -1;
        } else {
            left = x;
        }
        while (left > 0) {
            digits.add(left % 10);
            left /= 10;
        }
        try {
            while (digits.size() > 0) {
                reversed = Math.addExact(Math.multiplyExact(reversed, 10), digits.remove());
            }
            reversed = Math.multiplyExact(reversed, isPositive);
        } catch (ArithmeticException ex) {
            return 0;
        }
        return reversed;
    }
}