import java.util.ArrayList;
import java.util.Stack;

/* 
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

    1.Open brackets must be closed by the same type of brackets.
    2.Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.
*/
class Solution {
    public boolean isValid(String s) {
        Stack<Integer> parenthese = new Stack<>();
        Stack<Integer> square = new Stack<>();
        Stack<Integer> curly = new Stack<>();
        for (int i=0;i<s.length();i++) {
            char tmp = s.charAt(i);
            if (tmp == '(') parenthese.push(i);
            if (tmp == '[') square.push(i);
            if (tmp == '{') curly.push(i);
            int p = parenthese.size() == 0?0:parenthese.peek();
            int sq = square.size() == 0?0:square.peek();
            int c = curly.size() == 0?0:curly.peek();
            if (tmp == ')') {
                if (parenthese.size() > 0) {
                    if (p<sq && sq<tmp || p<c && c<tmp) {return false;}
                    parenthese.pop();
                }
                else return false;
            }
            else if (tmp == ']') {
                if (square.size() > 0) {
                    if (sq<p && p<tmp || sq<c && c<tmp) {return false;}
                    square.pop();
                }
                else return false;
            }
            else if (tmp == '}') {
                if (curly.size() > 0) {
                    if (c<p && p<tmp || c<sq && sq<tmp) {return false;}
                    curly.pop();
                }
                else return false;
            }
        }
        if (parenthese.size()==0 && square.size()==0 && curly.size() == 0)
        return true;
        else return false;
    }
}